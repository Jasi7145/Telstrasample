package com.telstra.util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Sample JUnit to test largest number
 * @author Jai
 *
 */
class TelstraUtilsTest {

	@Test
	void testGetLargestNumber() {
		Integer[] array = new Integer[] {12,13,42,56,90};
		
		Assertions.assertEquals(90, TelstraUtils.getLargestNumber(array));
		Assertions.assertNotEquals(42, TelstraUtils.getLargestNumber(array));
	}

}
