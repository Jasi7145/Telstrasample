package com.telstra.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model class to bind 
 * incoming json data
 * @author Jai
 *
 */
public class JSONModel {
	
	
	private String id;
	
	public JSONModel(String id, String findDuplicates, String whiteSpacesGalore,
			boolean validateMeOnlyIActuallyShouldBeABoolean, Integer[] numbersMeetNumbers) {
		super();
		this.id = id;
		this.findDuplicates = findDuplicates;
		this.whiteSpacesGalore = whiteSpacesGalore;
		this.validateMeOnlyIActuallyShouldBeABoolean = validateMeOnlyIActuallyShouldBeABoolean;
		this.numbersMeetNumbers = numbersMeetNumbers;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFindDuplicates() {
		return findDuplicates;
	}

	public void setFindDuplicates(String findDuplicates) {
		this.findDuplicates = findDuplicates;
	}

	public String getWhiteSpacesGalore() {
		return whiteSpacesGalore;
	}

	public void setWhiteSpacesGalore(String whiteSpacesGalore) {
		this.whiteSpacesGalore = whiteSpacesGalore;
	}

	public boolean isValidateMeOnlyIActuallyShouldBeABoolean() {
		return validateMeOnlyIActuallyShouldBeABoolean;
	}

	public void setValidateMeOnlyIActuallyShouldBeABoolean(boolean validateMeOnlyIActuallyShouldBeABoolean) {
		this.validateMeOnlyIActuallyShouldBeABoolean = validateMeOnlyIActuallyShouldBeABoolean;
	}

	public Integer[] getNumbersMeetNumbers() {
		return numbersMeetNumbers;
	}

	public void setNumbersMeetNumbers(Integer[] numbersMeetNumbers) {
		this.numbersMeetNumbers = numbersMeetNumbers;
	}

	private String findDuplicates;
	
	private String whiteSpacesGalore;
	
	private boolean validateMeOnlyIActuallyShouldBeABoolean;
	
	private Integer[] numbersMeetNumbers;
	
	

}
