package com.telstra.model;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Entity class to bind data to
 * mongo db collection
 * @author Jai
 *
 */

@Document(collection = "telstracollection")
public class JSONEntity {

	@JsonProperty("_ids")
	private String id;
	
	private String duplicates;
	
	private String sentense;
	
	private boolean validateMeOnlyIActuallyShouldBeABoolean;
	
	private Integer largestNumber;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDuplicates() {
		return duplicates;
	}

	public void setDuplicates(String duplicates) {
		this.duplicates = duplicates;
	}

	public String getSentense() {
		return sentense;
	}

	public void setSentense(String sentense) {
		this.sentense = sentense;
	}

	public boolean isValidateMeOnlyIActuallyShouldBeABoolean() {
		return validateMeOnlyIActuallyShouldBeABoolean;
	}

	public void setValidateMeOnlyIActuallyShouldBeABoolean(boolean validateMeOnlyIActuallyShouldBeABoolean) {
		this.validateMeOnlyIActuallyShouldBeABoolean = validateMeOnlyIActuallyShouldBeABoolean;
	}

	public Integer getLargestNumber() {
		return largestNumber;
	}

	public void setLargestNumber(Integer largestNumber) {
		this.largestNumber = largestNumber;
	}

	public JSONEntity(String id, String duplicates, String sentense, boolean validateMeOnlyIActuallyShouldBeABoolean,
			Integer largestNumber) {
		super();
		this.id = id;
		this.duplicates = duplicates;
		this.sentense = sentense;
		this.validateMeOnlyIActuallyShouldBeABoolean = validateMeOnlyIActuallyShouldBeABoolean;
		this.largestNumber = largestNumber;
	}

	
	
	
}
