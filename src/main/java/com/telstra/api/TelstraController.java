package com.telstra.api;

import java.util.Collection;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.model.JSONEntity;
import com.telstra.model.JSONModel;
import com.telstra.util.TelstraUtils;
import com.telstra.util.ValidationException;

/**
 * Controller class for api end points
 * @author Jai
 *
 */

@RestController
@RequestMapping("telstra/api")
public class TelstraController {
	
	private MongoTemplate mongoTemplate;
	
	public TelstraController(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}
	
	@PostMapping
	public void insert(@RequestBody JSONModel jsonModel) throws ValidationException {
		//Data type validation
		TelstraUtils.validateDataTypes(jsonModel);
		//Select largest number from array
		int number = TelstraUtils.getLargestNumber(jsonModel.getNumbersMeetNumbers());
		String duplicateString = TelstraUtils.findDuplicates(jsonModel.getFindDuplicates());
		String sentense = TelstraUtils.replaceWhiteSpaces(jsonModel.getWhiteSpacesGalore()); 
		
		JSONEntity jsonEntity = TelstraUtils.bindValues(jsonModel, number,duplicateString, sentense);
		
		this.mongoTemplate.insert(jsonEntity);
	}
	
	
	@GetMapping("/all")
	public  Collection<JSONEntity>  all (){
		
		Collection<JSONEntity> all = this.mongoTemplate.findAll(JSONEntity.class);
		return all;
		
	}
	

}
