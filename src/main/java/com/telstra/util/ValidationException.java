package com.telstra.util;

public class ValidationException extends Exception {
	
	public ValidationException(String s) {
		super(s);
	}

}
