package com.telstra.util;

import java.util.Objects;

import com.mongodb.lang.NonNull;
import com.telstra.model.JSONEntity;
import com.telstra.model.JSONModel;

/**
 * Helper class for validations and other stuff
 * 
 * @author Jai
 *
 */

public class TelstraUtils {

	public static void validateDataTypes(@NonNull JSONModel jsonModel) throws ValidationException {
		// TODO: Added simple validation for data types. This Can be done using
		// hibernate-validator aswell.
		try {

			int id = Integer.parseInt(jsonModel.getId());
			String flag = String.valueOf(jsonModel.isValidateMeOnlyIActuallyShouldBeABoolean());
			if (flag.equals(Boolean.TRUE) || Boolean.FALSE) {
				throw new ValidationException("Invalid Boolean Value");
			}

		} catch (NumberFormatException ne) {
			throw new ValidationException("Invalid JSON format");
		}

	}

	// Return largest number from int array
	public static int getLargestNumber(Integer[] array ) {
		// TODO Auto-generated method stub
		int max = array[0];
		for (int i = 1; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}

		System.out.print("Largest Number: " + max);
		return max;
	}

	// Returns duplicate string
	public static String findDuplicates(String str) {
		// TODO Auto-generated method stub
		int cnt = 0;
		char[] dups = str.toCharArray();
		char[] chrs = new char[dups.length];
		System.out.println("Duplicate Characters are:");
		for (int i = 0; i < str.length(); i++) {
			for (int j = i + 1; j < str.length(); j++) {
				if (dups[i] == dups[j]) {
					System.out.println(dups[j]);
					chrs[i] = dups[j];
					cnt++;
					break;
				}
			}
		}
		return new String(chrs);
	}

	// Removes whitespaces and returns same string
	public static String replaceWhiteSpaces(String sentence) {
		sentence = sentence.codePoints().filter(c -> !Character.isWhitespace(c))
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

		return sentence;

	}

	public static JSONEntity bindValues(JSONModel jsonModel, int largestNumber, String duplicate, String sentense) {
		// TODO Auto-generated method stub
		JSONEntity jsonEntiry = new JSONEntity(jsonModel.getId(), duplicate, sentense,
				jsonModel.isValidateMeOnlyIActuallyShouldBeABoolean(), largestNumber);
		return jsonEntiry;
	}

}
