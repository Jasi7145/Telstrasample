package com.telstra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampletelstraApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampletelstraApplication.class, args);
	}

}
